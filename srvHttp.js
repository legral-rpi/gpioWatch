#!/usr/bin/env node
/*!
fenBot
auteur: pascal TOLEDO
date creation: 2014.06.24
date modification: 2014.06.24
licence: GPLv3
*/

var FICHIER_NOM="srvHttp.js";
var PROJET_VER='0.1.0';
console.log('******************************************************');
t='demarrage de '+FICHIER_NOM+' ver:'+PROJET_VER;
console.log(t);
console.log('******************************************************');


// -- Chargement de le coloration de la console -- //
var colors = require('/usr/local/lib/node_modules/colors');
colors.setTheme({silly: 'rainbow',input: 'yellow',verbose: 'cyan',prompt:  'grey',info: 'green',data: 'blue',help: 'cyan',warn: 'yellow',debug: 'grey',error: 'red'});


/**********************************************
  ****  VARIABLES GLOBALES   **** 
***********************************************/
var DEV=(process.argv[2]=='dev=1')?1:0;	// Attention coder en dur
if(DEV===1){t="mode de develloppement";console.log(t.info);}
else{t="mode de production";console.log(t.info);}
// - path et fichiers - //
var ROOT='/www/projets/gpioWatch/';
var jqueryPath  = ROOT+'lib/tiers/js/jquery/jquery-1.8.1.min.js';
var gestLibJSPath   = ROOT+'lib/legral/js/gestLib/gestLib-0.1.js';
var gestLibCSSPath  = ROOT+'lib/legral/js/gestLib/gestLib-0.1.css';
var indexPath	= ROOT+'index.html';

var debug='err';	// changer via l'irc
debug=0;
var httpSrvr=null;
var HTTPPORT=13000,HTTPHOST='';
var bot=null;
var fileJSON='/dev/gpio.json';
t="front end http  \n";
console.log(t.info);

// - gestion des fichiers - //
var fileJSONContent='';	// recoit les donnees du fichier
var indexContent='';	// contenu du fichier index.html
var isjqueryLoad=0;	// le fichier est en memoire
var jqueryContent='';	// contenu du fichier jquery
var gestLibJSContent='';
var gestLibCSSContent='';

/**********************************************
  ****  CHARGERMENT/CREATION/CONNEXION   **** 
***********************************************/

var exec=require('child_process').exec;


/**********************************************
  ****  CHARGERMENT/CREATION/CONNEXION   **** 
***********************************************/


/**********************************************
  ==  httpSrv : creation du serveur ==
serveur http en liaison avec le client web
le client sera mise a jours via ajax
***********************************************/
//  - creation du serveur http - //
t="creation du server http ("+HTTPHOST+":"+HTTPPORT+"): reussi";console.log(t.debug);
var httpSrv = require('http').createServer(httpSrvImplement);
httpSrv.listen(HTTPPORT,HTTPHOST);


/**********************************************
  ==  gestion des fichiers  ==
**********************************************/
var fs=require('fs');

// - gestion du fichier fileJSON (/dev/gpio.json) - //
function lireFileJSON(err,data){
//	t="Lecture de "+fileJSON;console.log(t.info);
	if (err){t="erreur lors de la lecture de "+fileJSON;console.log(t.error);}
	//console.log(data);
	fileJSONContent=data;
	}

function  fileChange (curr,prev){
//	console.log("fichier changé");
	fs.readFile(fileJSON,{},lireFileJSON);
	}

// - charger le fichier index.html - //
function loadIndex(){
	t="Lecture de indexPath";console.log(t.info);
	indexContent=fs.readFileSync(indexPath);
	}

// - charger jquery - //
function loadjquery(){
	t="Lecture de "+jqueryPath;console.log(t.info);
        jqueryContent=fs.readFileSync(jqueryPath);
//	console.dir(jqueryContent);	// debug
	}

// - charger gestLib.js - //
function loadgestLibJS(){
        t="Lecture de "+gestLibJSPath;console.log(t.info);
        gestLibJSContent=fs.readFileSync(gestLibJSPath);
//        console.dir(gestLibJSContent);     // debug
        }

// - charger gestLib.css - //
function loadgestLibCSS(){
        t="Lecture de "+gestLibCSSPath;console.log(t.info);
        gestLibCSSContent=fs.readFileSync(gestLibCSSPath);
//        console.dir(gestLibJSContent);     // debug
        }


/**********************************************
         ****  MAIN   **** 
***********************************************/
// - charger les fichiers  - //
loadIndex();
loadjquery();
loadgestLibJS();
loadgestLibCSS();
fs.watchFile(fileJSON, {persistent:true,interval:1000},fileChange);

/**********************************************
         ****  IMPLEMENTATION   **** 
***********************************************/


/********************************************
Retourne le stdout formater selon un template
arg:
*options:
 - templateNu:
 - before
 - after
********************************************/
function stdoutTemplate(error,stdout,stderr,options){
	var t='';
	var opt=(typeof(options)=='object')?options:{};
	
	if(!opt.template)opt.template=0;
	if(!opt.before)opt.before='';
	if(!opt.after)opt.after='';
	if(!opt.cmd)opt.cmd='';
	
	var out='';
	switch(opt.template){
                case 0:
		case 'err':
                        out+=opt.before;
			if(opt.cmd!='')out+=opt.cmd+'\n';
                        out+=stdout;
			if(opt.template=='err'){out+='(error:'+error+')'}
			if(opt.template=='err'){out+='(stderr:'+stderr}
                        out+=opt.after;
                        return out;
                        break;
                }
	return stdout;

	}


/**********************************************
  ==  httpSrv : implementation  ==
serveur http en liaison avec le client web
 - envoie les donnees (le client les recoit via Ajax
***********************************************/
function httpSrvImplement(req, res){


	var t="httpSrvImplement";//console.log(t.debug);
        var psp=require('url').parse(req.url, true);

        var fileType='text/html',content="Aucune demande faite";

//	console.dir(psp.query['salon']);
	var salon=psp.query['salon']?psp.query['salon']:"ic-dijon";
//      console.dir(salon);
//	console.dir(psp);

        switch(psp.query['getData']){

	
		// - loadFile - //
		case 'jquery':     fileType='text/javascript';   content=jqueryContent;break;
		case 'gestLibJS':  fileType='text/javascript';   content=gestLibJSContent;break;
		case 'gestLibCSS': fileType='text/css';          content=gestLibCSSContent;break;

		case 'pinsDatas':	content=fileJSONContent;break;
		default:
			loadIndex();    // debug ou pas!
			content=indexContent;
               }
//	var head="<html><style>body{background-color:#000;color:#FFF}</style>";
//	content=head+content+"</html>";
//	t="content:"+content;console.log(t.data);
        res.writeHead(200,{"Content-Type": fileType});
        res.write(content);
        res.end();
//        t='les donnees ont ete envoyees';console.log(t.info)
//	t=content;console.log(t.info);
	}; // -- function httpSrvImplement
