/*
 * gpiowatch.c:
 lit les les etats des gpio et les ecrit dasn un fichier au format json
 ***********************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wiringPi.h>
//#include <jansson.h>

#define GPIOWATCHVERSION "v0.1"
#define FILESAVE "/dev/gpio.json"
FILE* fd = NULL;
char fdData[200+1];
char fdDataTmp[200+1];
char r[50+1];	//temporaire
#define DELAI 100


#define  PINNB 14
unsigned int pinsValue[PINNB];
unsigned int pinsValueLast[PINNB];
//unsigned int pinsUpCount[PINNB];
//unsigned int pinsDwCount[PINNB];

// - fonctions communes - //


void readPins(){
	// - Affiche la valeur des pins - //
	int pin;
	for (pin = 0 ; pin < PINNB ; ++pin){
//		readPin( pin );
		pinsValueLast[ pin ] = pinsValue[ pin ]; // on sauve l'etat actuel qui devient le precedent
		pinsValue[ pin ] = digitalRead(pin);

		// - on compte les fronts up et down - // 
//      if ( pinsValueLast[ pin ] == 0 && pinsValue[ pin ] ==1) pinsUpCount[ pin ]++;   // si down -> up
//      if ( pinsValueLast[ pin ] == 1 && pinsValue[ pin ] ==0) pinsDwCount[ pin ]++;   // si up -> down
		}
	}

int savePins(){
//	printf ("Ouverture de %s \n",FILESAVE);
	fd=fopen( FILESAVE , "w+" );

	if (fd != NULL){
		int pin;
		unsigned int val;
		unsigned int last;
//		unsigned int Up;
//		unsigned int Dw;

		strcpy(fdData,"{");

		// - chargement des valeurs - //
		for (pin = 0 ; pin < PINNB ; pin++){
			val=pinsValue[ pin ];
			last=pinsValueLast[ pin ];
//			Up= pinsUpCount[ pin ];
//			Dw= pinsDwCount[ pin ];

/*			printf("val:%u |",val);
			printf("last:%u |",last);
			printf("Up:%u |",Up);
			printf("Dw:%u \n",Dw);
*/
			sprintf(fdDataTmp,
//					"{\"No\" : %u, \"Val\" : %u, \"Last\" : %u, \"Up\" : %u, \"Dw\" : %u }\n"
//					,pin ,  val, last , Up , Dw
					"{\"No\" : %u, \"Val\" : %u }\n"
					, pin, val
				);
			printf(fdDataTmp); ///   DEBUG

			strcat(fdData,fdDataTmp);
			if ( pin < PINNB-1 ) strcat(fdData,","); // ajout de la virgule sauf au dernier
			}
		strcat(fdData,"}\n"); // on ferme les donnees json


		// - copie des donnes dans le fichier - //
		fputs(fdData,fd);
		fputs("\0",fd);   // zero terminal
	        fclose(fd);
		}
	else{
		printf("Ouverture de %s: erreur\n",FILESAVE);
		return -1;
		}
//	printf("DEBUG:fdData: %s ",fdData);
	return 1;
	}



/* - fonctions comportementales - */
/*
void pin03Vie(){
        int pin=3;
//        pinMode (pin, INPUT);
//        pin3Value=digitalRead (pin);
	readPin( pin );
        }


void pin08Vie(){
        int pin=8;
//        static int etat=0;
//        pinMode (pin, INPUT);
//        pin8Value=digitalRead (pin);
//        etat=(etat==0)?1:0;
	readPin( pin );
        }

void pin13Vie(){
	int pin=13;
        static int etat=0;
//	pinMode (pin, OUTPUT);
//        digitalWrite (pin, etat);
//        etat=(etat==0)?1:0;
	}


void pin11Vie(){
	int pin=11;
//	int etat=1;
//	etat=pin3Value;
	pinMode (pin, OUTPUT);
        digitalWrite (pin, pinsValue[ 3 ]);
//        etat=(etat==0)?1:0;
	}

void pin12Vie(){
        int pin=12;
        pinMode (pin, INPUT);
//        pin8Value=digitalRead (pin);
	readPin( pin );
        }
*/

/* - main - */
int main (void)
{

int pin ;
for ( pin = 0; pin < PINNB; ++pin){
	pinsValue[ pin ]=2;	// initialisation dans un etat non physique
	pinsValueLast[ pin ]=0;
//	pinsUpCount[ pin ]=0;
//	pinsDwCount[ pin ]=0;
	}


printf ("Raspberry Pi - GPIO Watch %s\n",GPIOWATCHVERSION) ;
printf ("==============================\n") ;
printf ("lis l'etat des GPIO") ;
printf ("et les ecrit dans un un fichier (en RAM)\n") ;
printf ("\n") ;

wiringPiSetup () ;


/* - Mise en etat de lecture - */
//printf ("mise en etat INPUT de ");
for (pin = 0 ; pin < PINNB ; ++pin){pinMode (pin, INPUT) ;printf ("%d.",pin);}
//printf ("\n");


pin=11;pinMode (pin, OUTPUT);digitalWrite (pin, 1);	// for debug

/* - loop - */

for (;;)
	{
//	pin08Vie();
//	pin13Vie();
//	pin11Vie();
//	pin12Vie();
	readPins();
	savePins();
	delay( DELAI ) ;
	}

pin=11;pinMode (pin, OUTPUT);digitalWrite (pin, 0);     // for debug

return 0 ;
}
