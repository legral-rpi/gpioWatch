#!/bin/sh
VERSION="0.1";
scriptPath=$0;           #chemin complet du script 
scriptRep=`dirname $0`; #repertoire du script

# - chargement des codes couleurs - #
source $scriptRep/couleur.sh

# - usage si pas de parametre - #
if [ $# -lt 2 ];then
	echo "$couleurINFO$repScript/gitVersion v$VERSION";
	echo "usage:"
	echo " gitVersion version commit";
	echo " gitVersion version push";
	echo " gitVersion version commit push";
	exit
	fi


function commit {

	echo "$couleurINFO on commit...";
	echo "mais avant:";
	echo "1- on copie la version dans le rep version.";
	if [ -f "./localVersion.sh" ];
	then
		echo "fichier ./localVersion.sh trouve.$couleurNORMAL";
		source ./localVersion.sh
		localSave;
	else
		echo "$couleurWARN pas de fichier ./localVersion.sh$couleurNORMAL";
		fi

	echo "$couleurINFO 2- on add .$couleurNORMAL";
	git add .

	echo "$couleurINFO maintenant on commit (copier le texte issue de release)$couleurNORMAL";
	git commit

	echo "$couleurINFO on tag le commit avec $couleurWARN$PROGVERSION$couleurNORMAL";
	git tag $PROGVERSION;
	}

function push {
	echo "$couleurINFO on push les commits$couleurNORMAL";
	git push;
	echo "$couleurINFO on push les tags$couleurNORMAL";
	git push --tag
	exit;
	} 

function sortieOk {
	echo "$couleurINFO on affiche le status:$couleurNORMAL"
	git st;
	echo "$couleurINFO on affiche les tags$couleurNORMAL";
	git tag;

	}


# - creer une copie en ajoutant la version en suffixe - #
# fn:fichier nom
# fe: fichier extention (sans le point)
# fv: nom du fichier avec la version en suffixe
# minSuf: suffixe de la minification
function versionSave {
	fn=$1;
	fe=".$2";	# ajout du point 
        fv="$fn-$PROGVERSION";
	echo "creation de la copie $fn$fe vers ./versions/$fv$fe";
        cp $fn$fe ./versions/$fv$fe
	}

function versionMinimise {
	fn=$1;
	fe=".$2";
        fv="$fn-$PROGVERSION";
	minSuf=".min";fm="$fv$minSuf$fe";
        echo "compression de  $fn$fe vers $fm$fe";
	java -jar ${scriptRep}/scripts/yuicompressor-2.4.8.jar $fn$fe -o ./versions/$fm$fe
        }


# - verification de l'existance des repertoires - #
if [ -d "./.git" ];
then
	echo "$couleurINFO repertoire ./.git existant$couleurNORMAL"
else
	echo "$couleurWARN repertoire ./.git NON existant -> creation d'un repo initial$couleurNORMAL"
	git init;
	fi

if [ -d "./versions" ];
then
        echo "$couleurINFO repertoire ./versions existant$couleurNORMAL"
else
        echo "$couleurWARN repertoire ./versions NON existant -> creation$couleurNORMAL"
	mkdir ./versions
        fi



######################################
PROGVERSION=$1;

if [ "$2" == "commit" ];then
	commit;
	if [ "$3" == "push" ];then
		push;
		fi
	sortieOk;
	fi

if [ "$2" == "push" ];then
	push;
	sortieOk;
	fi

