/*
 * gpiowatch.c:
 lit les etats des gpio et les ecrit dasn un fichier au format json
 ***********************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wiringPi.h>
//#include <jansson.h>
#define DEBUG 1

#define GPIOWATCHVERSION "v0.1.1"
#define FILESAVE "/dev/gpio.json"
FILE* fd = NULL;
char fdData[2000+1];
char r[50+1];	//temporaire
#define DELAI 100


#define  PINNB 14
int pinsValue[PINNB];
int pinsValueLast[PINNB];
unsigned int pinsUpCount[PINNB];
unsigned int pinsDwCount[PINNB];

// - fonctions communes - //


void readPins(){
	// - Affiche la valeur des pins - //
	int pin;
	for (pin = 0 ; pin < PINNB ; ++pin){
		pinsValueLast[ pin ] = pinsValue[ pin ]; // on sauve l'etat actuel qui devient le precedent
		pinsValue[ pin ] = digitalRead(pin);

		// - on compte les fronts up et down - // 
		if ( pinsValueLast[ pin ] == 0 && pinsValue[ pin ] ==1) pinsUpCount[ pin ]++;   // si down -> up
		if ( pinsValueLast[ pin ] == 1 && pinsValue[ pin ] ==0) pinsDwCount[ pin ]++;   // si up -> down
		}
	}

int savePins(){
//	printf ("Ouverture de %s \n",FILESAVE);
	fd=fopen( FILESAVE , "w+" );

	if (fd != NULL){
		int pin;
		int val;
		int last; // derniere valeur de val
		unsigned int Up;
		unsigned int Dw;

		char fdDataTmp0[200+1];
		char fdDataTmp1[200+1];

		strcpy(fdData,"[");

		// - chargement des valeurs - //
		for (pin = 0 ; pin < PINNB ; pin++){
			val=pinsValue[ pin ];
			last=pinsValueLast[ pin ];
			Up=pinsUpCount[ pin ];
			Dw=pinsDwCount[ pin ];


			strcpy(fdDataTmp0,"{");
			sprintf(fdDataTmp1,"\"No\" : %d",   pin);strcat(fdDataTmp0,fdDataTmp1);

			sprintf(fdDataTmp1,",\"Val\" : %d", val);strcat(fdDataTmp0,fdDataTmp1);

			// BUG : a partir d'ici les retours sont anarchiques!
			sprintf(fdDataTmp1,",\"last\" : %d",last);strcat(fdDataTmp0,fdDataTmp1);
			sprintf(fdDataTmp1,",\"Up\" : %u",  Up);  strcat(fdDataTmp0,fdDataTmp1);
			sprintf(fdDataTmp1,",\"Dw\" : %u",  Dw);  strcat(fdDataTmp0,fdDataTmp1);

			strcat(fdDataTmp0,"}\n");

			strcat(fdData,fdDataTmp0);
			if ( pin < PINNB-1 ) strcat(fdData,","); // ajout de la virgule sauf au dernier

			if (DEBUG==1)printf(fdData); ///   DEBUG
			}
		strcat(fdData,"]\n"); // on ferme les donnees json


		// - copie des donnes dans le fichier - //
		fputs(fdData,fd);
		fputs("\0",fd);   // zero terminal
	        fclose(fd);
		}
	else{
		printf("Ouverture de %s: erreur\n",FILESAVE);
		return -1;
		}
//	printf("DEBUG:fdData: %s ",fdData);
	return 1;
	}


/* - main - */
int main (void)
{

int pin ;
for ( pin = 0; pin < PINNB; ++pin){
	pinsValue[ pin ]=2;	// initialisation dans un etat non physique
	pinsValueLast[ pin ]=0;
	pinsUpCount[ pin ]=0;
	pinsDwCount[ pin ]=0;
	}


printf ("Raspberry Pi - GPIO Watch %s\n",GPIOWATCHVERSION) ;
printf ("==============================\n") ;
printf ("lis l'etat des GPIO") ;
printf ("et les ecrit dans un un fichier (en RAM)\n") ;
printf ("\n") ;

wiringPiSetup () ;


/* - Mise en etat de lecture - */
printf ("mise en etat INPUT de ");
for (pin = 0 ; pin < PINNB ; ++pin){pinMode (pin, INPUT) ;printf ("%d.",pin);}
printf ("\n");


pin=11;pinMode (pin, OUTPUT);digitalWrite (pin, 1);	// for debug

/* - loop - */

for (;;)
	{
	readPins();
	savePins();
	delay( DELAI ) ;
	}

pin=11;pinMode (pin, OUTPUT);digitalWrite (pin, 0);     // for debug

return 0 ;
}
